1. ## Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

- Grit is the perseverance to achieve what you want to accomplish in life.

- Treat life as a marathon, not a sprint.

2. ## Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

- Fixed Mindset believes that skills are predefined and cannot be improved, while Growth Mindset believes that skills can be improved.

- For Fixed Mindset, they do not see the value of challenges and feedback while Growth Mindset does.

3. ## What is the Internal Locus of Control? What is the key point in the video?

- refers to an individual's belief or perception about the extent to which they have control or influence over their own life and the events that happen to them.

- Believe you have an internal locus of control by keeping works that can be done by you and it will make you stay motivated.

4. ## What are the key points mentioned by speaker to build growth mindset (explanation not needed).

- Believe in your ability to figure things out.

- Question your assumptions

- Create your own curriculum for growth

- Honor the struggle

5. ## What are your ideas to take action and build Growth Mindset?

- Start with simple problems and then increase the difficulty.

- After hitting a roadblock, look through your previous work to find whether there is a clue for the current problem.

- Look through some tutorials and ask others to find hints which will eventually lead to the solution.

- Solve it successfully and celebrate for it and then go forward.