1. ## Which point(s) were new to you?

- You should strongly consider blocking social media sites and apps during work hours. Tools like TimeLimit, Freedom, or any other app can be helpful.

- We recommend tracking your time using app like Boosted to improve productivity.

- Use Github gists to share code snippets. Use sandbox environments like Codepen, Codesandbox to share the entire setup

- Look at the way issues get reported in large open-source projects.

2. ## Which area do you think you need to improve on? What are your ideas to make progress in that area?

Asking to clear some doubts.

- Make a clear and concise way to tell what is the doubt you are having and explain it in such a way that they could understand easily.

- Do not stutter while asking.