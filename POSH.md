## What kinds of behavior cause sexual harassment?

- Sexual and pervasive jokes.
- Vulgar comments about another's body.
- Unnecessary and inappropriate body-touching.

## What would you do in case you face or witness any incident or repeated incidents of such behavior?

1. **Assess the situation first.**
   - Determine the nature and severity of the behavior.

2. **Report to Human Resources for extreme behavior.**
   - If the behavior is severe, involving HR or management may be necessary.

3. **Ensure the safety of the victim.**
   - Prioritize the safety and well-being of the person affected by the behavior.

4. **Seek support or get support.**
   - Reach out to colleagues, friends, or authorities who can assist you.

5. **Talk with the person and ask him/her to not make inappropriate jokes in the office and explain why it is not okay to do it.**
   - Address the behavior directly if safe to do so, educate the person about the inappropriateness of their actions.
