1. ### What are the activities you do that make you relax - Calm quadrant?

- Playing Games.

- Listening to music.

- Watching shows.

- Reading Comics.

2. ### When do you find getting into the Stress quadrant?

- When I cannot find a solution to a question.

- When my deadline is short and my work left is too much.

- When I have an assessment or review.

- When I made a decision which I later realize that it was not a good one.

3. ### How do you understand if you are in the Excitement quadrant?

- When I find the answer to the question which I spent too much time spending on.

- When I achieved something after struggling a lot.

- When I play any sports.

- When I exercise.

4. ### Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

- Sleep increases brain activity.

- Memory gets worse as your sleep gets worse.

- Learning Rate is more for those who sleep well than those that do not.

- Immune Cell activity decreases with decrease in sleep.

5. ### What are some ideas that you can implement to sleep better?

- Listening to musics that bring a peace of mind.

- Comfortable Sleep environment.

- Consistent Sleep schedule.

- Stay active.

6. ### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

- Ability to shift and focus attention by exercise.

- Makes brain more active.

- No morbid thoughts.

- Less depression.

- Be happier.

7. ### What are some steps you can take to exercise more?

- Set Goals.

- Start Slowly.

- Stay consistent.

- Stay Motivated.