## What are the steps/strategies to do Active Listening? (Minimum 6 points)

- Give full attention

- Maintain eye contact

- Use non-verbal cues

- Avoid interrupting

- Provide feedback

- Show Respect

## According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

- Listen Carefully

- Repeat and paraphrase

- No judgement

- Emphasize

- Ask clarifying questions

- Support and validate

## What are the obstacles in your listening process?

- Selective Listening

- Information Overboard

- Distractions

- Bias and prejudice

## What can you do to improve your listening?

- Give full attention

- Be open-minded

- Practice patience

- Reflect back

- Take notes

- Seek feedback

## When do you switch to Passive communication style in your day to day life?

- When I need to use something at the moment but someone else is using it.

- When I want to ask a question to clarify something but the moment of the topic has already passed.

- When you need something from someone, but the other guy is busy with something else.

## When do you switch into Aggressive communication styles in your day to day life?

- When waiting in a line for something for a long time and someone else cuts the line before.

- When having to complain about something after having a bad day.

- When arguing with friends which goes in the negative direction.

## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- When had a verbal fight with friends and family members.

- When getting frustrated about someone about something.

## How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

- Talking to your closest friend about your problems or a friend about your problems who won't get already irritated.Even family members (they are the safest zones for me)

- Recognise exactly what it is you are feeling and try to find a solution.

- Start with small things. Like asking friendly waiter about your order.