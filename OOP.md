![oop](./oop.jpeg)
# Object-Oriented Programming (OOP)

Object-Oriented Programming (OOP) is a programming paradigm that uses objects and classes to model and structure code. It is based on several key theoretical concepts:

## 1. **Classes and Objects**

   - **Class**: A class is a blueprint or template for creating objects. It defines the attributes (data) and methods (functions) that objects of the class will have.

    Example:
  ```python
  class Person:
      def __init__(self, name, age):
          self.name = name
          self.age = age
  ```
   
   - **Object**: An object is an instance of a class. It represents a real-world entity and encapsulates both data (attributes) and behavior (methods).

    Example:

   ```
   person1 = Person("Alice", 30)
   person2 = Person("Bob", 25)
   ```

## 2. **Inheritance**

   Inheritance is a mechanism that allows a new class (subclass or derived class) to inherit attributes and methods from an existing class (base class or superclass). It promotes code reuse and hierarchy in class structures.

   Example:
   ```
   class Student(Person):
    def __init__(self, name, age, student_id):
        super().__init__(name, age)
        self.student_id = student_id
   ```

## 3. **Encapsulation**

   Encapsulation restricts direct access to some of an object's components, protecting the integrity of an object's state. This often involves the use of access modifiers like private and public to control access to attributes and methods.

   Example:
   ```
   class BankAccount:
    def __init__(self, balance=0):
        self.__balance = balance  # Encapsulated balance attribute

    def deposit(self, amount):
        self.__balance += amount
   ```

## 4. **Polymorphism**

   Polymorphism allows objects of different classes to be treated as objects of a common superclass. It facilitates flexibility in method calls and enables dynamic method dispatch.

   Example:

   ```
   def introduce(person):
    print(f"Hello, my name is {person.name}.")

alice = Person("Alice", 30)
bob = Student("Bob", 20, "12345")

introduce(alice)  # Calls introduce with a Person object
introduce(bob)    # Calls introduce with a Student object
   ```

## 5. **Abstraction**

   Abstraction simplifies complex reality by modeling classes based on essential properties and behaviors while hiding unnecessary details. It often involves defining abstract classes or interfaces that declare common behaviors.

   Example:

   ```
   from abc import ABC, abstractmethod

class Shape(ABC):
    @abstractmethod
    def area(self):
        pass
   ```

These OOP concepts are fundamental to organizing code in a modular, reusable, and maintainable way. OOP encourages the modeling of real-world entities, making it a powerful and widely used paradigm in software development.

### References

- [Wikipedia](https://en.wikipedia.org/wiki/Object-oriented_programming)
- [GeeksForGeeks](https://www.geeksforgeeks.org/python-oops-concepts/)
