# Life Skills

1. ### What is the Feynman Technique? Explain in 1 line.

- The Feynman Technique is a learning and study method that involves simplifying complex concepts by explaining them as if you were teaching them to a child which helps you identify gaps in your understanding, refine your knowledge, and improve your retention of the material.

2. ### In this video, what was the most interesting story or idea for you?

- How she went from a girl who used to flunk math tests to joining military to learn language and roaming around the world to being a professor in engineering.

3. ### What are active and diffused modes of thinking?

- Active mode is when you are focused and concentrated on a specific task making you think analytically, while diffuse mode is when you relaxed and unconcentrated becoming creative while also having a broad perspective.

4. ### According to the video, what are the steps to take when approaching a new topic? Only mention the points.

- Deconstruct the skill

- Learn enough to self-correct

- Remove practice barriers

- Practice at least 20 hours

5. ### What are some of the actions you can take going forward to improve your learning process?

- Make a list of what needs to be done for the day.

- Take small breaks from long hours of work so that our brain is not overloaded and effiency increases.

- Listen to music during breaks to keep our mind fresh to start the next work.

- If you want walk a little around the surrounding area.

- Keep distractions at the minimum during work hours.

- Focus on the concept and approach it with curiosity and joy.
