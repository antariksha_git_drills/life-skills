1. ## In this video, what was the most interesting story or idea for you?

- Do some small things after doing your usual routine like doing 2 push-ups after flossing teeth, which will lead to push-ups being one of your daily habits from tiny habits.

2. ## How can you use B = MAP to make making new habits easier? What are M, A and P.

- Become more motivated, sharpen your skills and abilities and set up reminders to perform habits at the right time.

- M: Motivation

- A: Ability

- P: Prompts

3. ## Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

- It gives us more motivation to perform other tasks.

- Gives positive reinforcements.

- Maintains self-confidence.

- Reduces stress and anxiety.

4. ## In this video, what was the most interesting story or idea for you?

- Good habits make time your ally, bad habits, make time your enemy and there are 4 stages of good habits which are noticing, wanting, doing and liking.

5. ## What is the book's perspective about Identity?

- The ultimate form of our intrinsic motivation is when a habit becomes a part of our identity.

- We need change our identity to solve problems for longer term at the system level.

6. ## Write about the book's perspective on how to make a habit easier to do?

- Cue: Trigger the brain to initiate an action.

- Craving: Provides motivational force.

- Response: Action or habit we perform.

- Reward: End Goal which should be immediately satisfying.

7. ## Write about the book's perspective on how to make a habit harder to do?

- Create a new habit completely independent of the regular habits without any obvious cues.

- Not getting enough motivation to do the habit.

- The reward is not immediate satisfying after finishing a habit.

8. ## Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

Doing exercises.

    Coming home, changing to regular clothes and turn on music to get motivation to  to exercise and after a simple exercise, eat a snack like a protein bar to make the habit satisfying.

9. ## Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

Biting my nails

    Use a nail cutter to cut your nails regularly so that biting would be harder.Think about the risk of bleeding fingers if you bite too deep.